# README #

This respository is just to keep a backup of reusable code from [the website](https://blingvine.com). It is used to sell one gram gold jewellery in India. [BlingVine](https://blingvine.com) an online shop with online payment processing and inventory management.

### What does this website support? ###

* Online payments
* Jewellery Inventory Management
* Cash on Delivery
* Customer Support